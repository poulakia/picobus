# Picobus
An 8p PocketQube deployer

## Deployer PCB
For this kicad project you will need the [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib) library.

## License

Licensed under the [CERN OHLv1.2](LICENSE) 2019 Libre Space Foundation.
